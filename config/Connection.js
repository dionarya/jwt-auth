const Sequelize = require('sequelize');
const connection = new Sequelize('cl_mahasiswa', 'root', '', {
    host: '127.0.0.1',
    dialect: 'mysql',
    port: 3306,
    operatorsAliases: false
});

module.exports = {
    seq: Sequelize,
    conn : connection
}