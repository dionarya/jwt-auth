const con = require('./../config/Connection');
const Statuses = con.conn.define('statuses', {
    id: {
            type: con.seq.INTEGER,
            primaryKey: true,
            autoIncrement: true 
        },
    isi: con.seq.STRING,
    userId: con.seq.STRING
});

module.exports = Statuses;