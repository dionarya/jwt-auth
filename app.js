const express    = require('express');
const jwt        = require('jsonwebtoken');
const superSec   = 'helohelohelobandung';
const app        = express();
const bodyParser = require('body-parser');
const User       = require('./models/User');
const Statuses  = require('./models/Statuses');
const bcrypt     = require('bcrypt-nodejs');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send("Welcome Bror");
});

app.get('/api', (req, res) => {
  res.json({
    message: 'Welcome to the API'
  });
});

app.post('/api/posts', verifyToken, (req, res) => {  
  jwt.verify(req.token, superSec, (err, authData) => {
    if(err) {
      res.sendStatus(403);
    } else {
      res.sendStatus(200);
      res.json({
        message: 'Post created...',
        authData
      });
    }
  });
});

app.get('/getmahasiswa', verifyToken, (req, res) => {
  Statuses.all().then(Mahasiswa => {
      if(!Mahasiswa){
        res.json({
            status: 404,
            message: "mahasiswa not Found"
        });
    }else{
        res.json({
            status: 200,
            message: "mahasiswa Found",
            Mahasiswa
        });
     }
    });
});

app.post('/api/login', (req, res) => {
  User.findOne({where: {username: req.body.username}}).then(user => {
    if(!user){
      res.sendStatus(404);
    }else{
        bcrypt.compare(req.body.password, user.passsword, function(err, result){
          if(err){
            res.sendStatus(404)
          }else if(result){
          jwt.sign({user}, superSec, { expiresIn: '30s' }, (err, token) => {
              res.json({token});
          });
        }else{
          res.sendStatus(404);
        }    
      });
    }
  });
});

// Verify Token
function verifyToken(req, res, next) {
  const bearerHeader = req.headers['authorization'];
  if(typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];
    req.token = bearerToken;
    next();
  } else {
    res.sendStatus(403);
  }
}


app.listen(5000, () => console.log('Server started on port 5000'));